package com.example.sqlgilces;

import com.orm.SugarRecord;

public class Book extends SugarRecord {
    String title;
    String edition;
    String numeropasajeros;
    String recorridos;
    String viaje;

    public Book(){
    }

    public Book(String title, String edition, String numeropasajeros, String recorridos, String viaje) {
        this.title = title;
        this.edition = edition;
        this.numeropasajeros = numeropasajeros;
        this.recorridos = recorridos;
        this.viaje = viaje;



    }


    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() { return edition; }
    public void setEdition(String edition) { this.edition = edition; }


    public String getNumeropasajeros() {
        return numeropasajeros;
    }
    public void setNumeropasajeros(String numeropasajeros) {  this.numeropasajeros = numeropasajeros;  }


    public String getRecorridos() {
        return recorridos;
    }
    public void setRecorridos(String recorridos) {   this.recorridos = recorridos;}

    public String getViaje() { return viaje; }
    public void setViaje(String viaje) {   this.viaje = viaje;


    }


}
