package com.example.sqlgilces;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class edit extends AppCompatActivity {
    EditText txtTitle, txtEdit;
    Button btnApply, consultagenera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        txtTitle = (EditText) findViewById(R.id.txt_ttl);
        txtEdit = (EditText) findViewById(R.id.txt_edi);


        final Bundle bundle = this.getIntent().getExtras();
        Log.e("identificador", bundle.getString("id"));

        btnApply = (Button) findViewById(R.id.btn_aplicar);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong(bundle.getString("id")));
                Log.e("identificador", bundle.getString("id"));
                book.title = txtTitle.getText().toString();
                book.edition = txtEdit.getText().toString();
                book.save();
            }
        });

        consultagenera = (Button) findViewById(R.id.btn_consulta);
        consultagenera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(edit.this, listView.class);
                startActivity(i);
            }
        });
    }
}
