package com.example.sqlgilces;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class listView extends AppCompatActivity {
    ListView listaBook;
    Book book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listaBook = (ListView) findViewById(R.id.lv_books);

        final List<Book> books = Book.listAll(Book.class);
        final List<String> listaTextoLibros = new ArrayList<>();

        for(Book libro: books){
            Log.e("Libro", libro.getTitle() + ", " + libro.getEdition()+ ", "+libro.getNumeropasajeros()+ ","+libro.getRecorridos()+","+libro.getViaje());
            listaTextoLibros.add(libro.getTitle() + ", " + libro.getEdition() + ", "+libro.getNumeropasajeros()+", "+libro.getRecorridos()+", "+libro.getViaje());
        }



        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, listaTextoLibros);

        listaBook.setAdapter(itemsAdapter);
        listaBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent edit = new Intent(listView.this, edit.class);
                Bundle ident = new Bundle();
                edit.putExtras(ident);
                startActivity(edit);
            }
        });
    }
}




