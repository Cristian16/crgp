package com.example.sqlgilces;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listViewLibros;
    Button btnWrite;
    Button btnRead;
    Button btnDelete, btnConsultaInd, btnDeleteAll;
    Button btnEdit;
    EditText txtTitle, txtEdition, txtConsultaInd,  txtpasajeros,  txtrecorrido,  txtviaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConsultaInd = (Button) findViewById(R.id.btn_consultaIndividual);
        listViewLibros =(ListView) findViewById(R.id.lv_books);

        btnWrite = (Button) findViewById(R.id.btn_write);
        btnRead = (Button) findViewById(R.id.btn_read);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnEdit = (Button) findViewById(R.id.btn_edit);

        txtConsultaInd= (EditText) findViewById(R.id.editxt_update);
        txtTitle = (EditText) findViewById(R.id.txt_horafecha);
        txtEdition = (EditText) findViewById(R.id.txt_nombrebote);
        txtpasajeros = (EditText) findViewById(R.id.txt_pasajero);
        txtrecorrido = (EditText) findViewById(R.id.txt_recorrido);
        txtviaje = (EditText) findViewById(R.id.txt_razonviaje);

        btnDeleteAll = (Button) findViewById(R.id.btn_delete_all);

        btnWrite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Book registro = new Book(txtTitle.getText().toString(), txtEdition.getText().toString(), txtpasajeros.getText().toString(),
                                        txtrecorrido.getText().toString(),  txtviaje.getText().toString());
                registro.save();
                Toast.makeText(MainActivity.this,"Guardado con exito",Toast.LENGTH_LONG).show();

            }
        });

        btnRead.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, listView.class);
                startActivity(i);
            }
        });

        btnConsultaInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong(txtConsultaInd.getText().toString()));
                txtTitle.setText(book.getTitle());
                txtEdition.setText(book.getEdition());
                txtpasajeros.setText(book.getNumeropasajeros());
                txtrecorrido.setText(book.getRecorridos());
                txtviaje.setText(book.getViaje());


            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong(txtConsultaInd.getText().toString()));
                book.title = txtTitle.getText().toString();
                book.edition = txtEdition.getText().toString();
                book.numeropasajeros = txtpasajeros.getText().toString();
                book.recorridos = txtrecorrido.getText().toString();
                book.viaje = txtviaje.getText().toString();
                book.save();
            }
        });

        btnDeleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Book> books = Book.listAll(Book.class);
                Book.deleteAll(Book.class);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong(txtConsultaInd.getText().toString()));
                book.delete();
            }
        });


    }
}