package com.example.calculosueldo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText ingresarhoras;
    Button calcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ingresarhoras = (EditText) findViewById(R.id.ingresarhoras);
        calcular = (Button) findViewById(R.id.calcular);
        calcular.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                // se genera la navegabilidad entre la actividad principal y la actividad
                Intent intent = new Intent(MainActivity.this, Pantalla2.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", ingresarhoras.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });



    }
}
