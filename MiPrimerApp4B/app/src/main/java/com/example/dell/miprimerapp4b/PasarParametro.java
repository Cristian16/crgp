package com.example.dell.miprimerapp4b;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {

    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        cajaDatos = (EditText) findViewById(R.id.txtParametro);
        botonEnviar = (Button) findViewById(R.id.btnEnviar);
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametro.this, RecivirParametro.class);
                // se crea un onjeto de tipo bundle que será la bitacora de parametros a enviar
                Bundle bundle = new Bundle();

                bundle.putString("dato", cajaDatos.getText().toString());
                //metodo putExtras envia un objeto de tipo bundle como un solo parametro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}