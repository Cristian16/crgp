package com.example.dell.miprimerapp4b;


import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener,FrgDos.OnFragmentInteractionListener{

    Button botonFrgUno, botonFrgDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonFrgDos =(Button) findViewById(R.id.btnFrgDos);
        botonFrgDos =(Button) findViewById(R.id.btnFrgUno);
        botonFrgUno.setOnClickListener(this);
        botonFrgDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FrgUno fragmentoUno = new FrgUno();
                FragmentTransaction transaccionUno = getSupportFragmentManager().beginTransaction();
                transaccionUno.replace(R.id.contenedor, fragmentoUno);
                transaccionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transaccionDos = getSupportFragmentManager().beginTransaction();
                transaccionDos.replace(R.id.contenedor, fragmentoDos);
                transaccionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
