package com.example.dell.practicas8_4b_crgp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button botonMemoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonMemoria = (Button) findViewById(R.id.btnMemoria);
        botonMemoria.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent inte = new Intent(this, Memoria_Interna.class);
        startActivity(inte);

    }
}

